package edu.byui.derekrl.w05;

public class Calculator {

    private Float input1;
    private Float input2;
    private Integer operation;

    public Calculator(Float input1, Float input2, Integer operation) {
        this.input1 = input1;
        this.input2 = input2;
        this.operation = operation;
        // operations: 1+, 2-, 3*, 4/
    }

    public Float getInput1() {
        return input1;
    }

    public void setInput1(Float input1) {
        this.input1 = input1;
    }

    public Float getInput2() {
        return input2;
    }

    public void setInput2(Float input2) {
        this.input2 = input2;
    }

    public void setInputs(Float input1, Float input2) {
        this.input1 = input1;
        this.input2 = input2;
    }

    public Integer getOperation() {
        return operation;
    }

    public void setOperation(Integer operation) {
        this.operation = operation;
    }

    public void validateOperation() {
        if(this.operation < 1 || this.operation > 4) {
            throw new RuntimeException("Operation not in range");
        }
        if(this.operation.equals(4) && this.input2.equals(0f)) {
            throw new ArithmeticException("Cannot divide by zero");
        }
    }

    public Float runCalculation() throws ArithmeticException {
        float result;
        switch (this.operation) {
            case 1 -> result = this.input1 + this.input2;
            case 2 -> result = this.input1 - this.input2;
            case 3 -> result = this.input1 * this.input2;
            case 4 -> result = this.input1 / this.input2;
            default -> throw new RuntimeException("Operation not in range");
        }
        return result;
    }



}
