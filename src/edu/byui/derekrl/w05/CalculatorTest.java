package edu.byui.derekrl.w05;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {

    Calculator c = new Calculator(1f, 1f, 1);

    @Nested
    @DisplayName("Getters and Setters")
    class GettersAndSetters {

        @Test
        @DisplayName("Setters")
        void testSetters() {
            c.setInput1(1f);
            c.setInput2(2f);
            c.setInputs(3f, 4f);
            c.setOperation(3);
        }

        @Test
        @DisplayName("Getters")
        void testGetters() {
            Calculator c = new Calculator(1f, 2f, 3);
            assertEquals(1f, c.getInput1(), 0.001);
            assertEquals(2f, c.getInput2(), 0.001);
            assertEquals(3, c.getOperation());
        }
    }

    @Nested
    @DisplayName("Test for meta errors")
    class MetaErrors {

        @Test
        @DisplayName("Operation ranges")
        void operationRange() {
            c.setOperation(0);
            assertThrows(RuntimeException.class, () -> c.validateOperation());
            assertThrows(RuntimeException.class, () -> c.runCalculation());

            c.setOperation(5);
            assertThrows(RuntimeException.class, () -> c.validateOperation());
            assertThrows(RuntimeException.class, () -> c.runCalculation());

            c.setOperation(1);
            c.validateOperation();
            c.runCalculation();
        }

        @Test
        @DisplayName("Divide by zero")
        void divideByZero() {
            c.setInputs(1f, 0f);
            c.setOperation(4);

            assertThrows(ArithmeticException.class, () -> c.validateOperation());
        }

    }

    @Nested
    @DisplayName("Test calculations")
    class Calculations {

        @Test
        @DisplayName("Addition")
        void addition() {
            c.setOperation(1);

            c.setInputs(2f,4f);
            assertEquals(c.runCalculation(), 6f, 0.001);

            c.setInputs(3.14159f,2f);
            assertEquals(c.runCalculation(), 5.14159f, 0.001);
        }

        @Test
        @DisplayName("Subtraction")
        void subtraction() {
            c.setOperation(2);

            c.setInputs(2f,4f);
            assertEquals(c.runCalculation(), -2f, 0.001);

            c.setInputs(3.14159f,2f);
            assertEquals(c.runCalculation(), 1.14159f, 0.001);
        }

        @Test
        @DisplayName("Multiplication")
        void multiplication() {
            c.setOperation(3);

            c.setInputs(2f,4f);
            assertEquals(c.runCalculation(), 8f, 0.001);

            c.setInputs(3.14159f,2f);
            assertEquals(c.runCalculation(), 6.28318f, 0.001);
        }

        @Test
        @DisplayName("Division")
        void division() {
            c.setOperation(4);

            c.setInputs(2f,4f);
            assertEquals(c.runCalculation(), 0.5f, 0.001);

            c.setInputs(3.14159f,2f);
            assertEquals(c.runCalculation(), 1.570795f, 0.001);

            c.setInputs(7f,3f);
            assertNotEquals(c.runCalculation(), 2f, 0.1);
        }

    }
}


