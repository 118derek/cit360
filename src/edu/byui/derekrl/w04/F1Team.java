package edu.byui.derekrl.w04;

public class F1Team {

    private String name;
    private String engine;
    private String driver1;
    private String driver2;
    private Double points;

    public F1Team() {
    }

    public F1Team(String name, String engine, String driver1, String driver2, Double points) {
        this.name = name;
        this.engine = engine;
        this.driver1 = driver1;
        this.driver2 = driver2;
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public String getEngine() {
        return engine;
    }

    public String getDriver1() {
        return driver1;
    }

    public String getDriver2() {
        return driver2;
    }

    public Double getPoints() {
        return points;
    }

    @Override
    public String toString() {
        return String.format("""
                -- F1 Car --
                Team name: %s
                Engine manufacturer: %s
                First driver's name: %s
                Second driver's name: %s
                Points so far this season: %f""",
                name, engine, driver1, driver2, points);
    }
}
