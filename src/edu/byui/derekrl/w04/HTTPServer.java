package edu.byui.derekrl.w04;

import java.io.*;
import java.net.*;
import com.sun.net.httpserver.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class HTTPServer {

    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
        HttpContext team = server.createContext("/team");
        team.setHandler(HTTPServer::handleTeamRequest);
        server.start();
        System.out.println("HTTP server started on port 8080");
    }

    private static void handleTeamRequest(HttpExchange exchange) throws IOException {

        F1Team f1Team = new F1Team("Red Bull Racing",
                "Honda",
                "Max Verstappen",
                "Sergio Perez",
                364.5);

        String response = F1TeamToJSON(f1Team);

        exchange.sendResponseHeaders(200, response.getBytes().length);
        OutputStream output = exchange.getResponseBody();
        output.write(response.getBytes());
        output.close();
    }

    public static String F1TeamToJSON(F1Team obj) {

        ObjectMapper mapper = new ObjectMapper();
        String str = "";

        try {
            str = mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return str;
    }



}
