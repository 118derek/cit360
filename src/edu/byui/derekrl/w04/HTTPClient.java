package edu.byui.derekrl.w04;

import java.net.*;
import java.io.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class HTTPClient {

    public static void main(String[] args) {

        String url = "http://localhost:8080/team";

        Integer responseCode = HTTPClient.getHTTPResponseCode(url);

        if (responseCode == 200) {
            String json = HTTPClient.getHTTPContent(url);
            F1Team object = JSONtoF1Team(json);
            System.out.println(object.toString());
        } else {
            System.err.println("No OK response from server");
        }

    }

    public static String getHTTPContent(String input) {

        try {
            URL url = new URL(input);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder builder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                builder.append(line).append("\n");
            }

            return builder.toString();

        } catch (IOException e) {
            System.err.println(e.toString());
        }

        return "Something went wrong";

    }

    public static Integer getHTTPResponseCode(String input) {

        try {

            URL url = new URL(input);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            return http.getResponseCode();

        } catch(IOException e) {
            System.err.println(e.toString());
        }

        return 418;

    }

    public static F1Team JSONtoF1Team(String input) {

        ObjectMapper mapper = new ObjectMapper();
        F1Team object = null;

        try {
            object = mapper.readValue(input, F1Team.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return object;
    }

}
