package edu.byui.derekrl.w02;

import java.util.*;

public class FourCollections {

    public static void main(String[] args) {

        System.out.println("---[ List: ArrayList ]---\n- Descriptions of collection types to cover -");

        List<TypeString> collectionTypes = new ArrayList<>();
        collectionTypes.add(new TypeString("TreeSet: No duplicates, self-sorts"));
        collectionTypes.add(new TypeString("Queue: Generally ordered FIFO"));
        collectionTypes.add(new TypeString("Set: No duplicates, not required to be ordered"));
        collectionTypes.add(new TypeString("Map: Object/key pairs, duplicate keys replace data"));
        collectionTypes.add(new TypeString("List: Ordered, duplicates allowed"));

        for (TypeString str : collectionTypes) {
            System.out.println(str);
        }

        TypeStringCompare tmc = new TypeStringCompare();
        Collections.sort(collectionTypes, tmc);

        System.out.println("\n- Again, but sorted in alphabetical order -");

        for (TypeString str : collectionTypes) {
            System.out.println(str);
        }

        System.out.println("\n---[ Queue: LinkedList w/ Generics ]---\n- Tracks in Big Box EP -");

        Queue<MusicTracks> trackNames = new LinkedList<>();
        trackNames.offer(new MusicTracks(1, "Lost Empire", "Ahoy"));
        trackNames.offer(new MusicTracks(2, "Room to Breathe", "Ahoy"));
        trackNames.offer(new MusicTracks(3, "Tangible Dimension", "Ahoy"));
        trackNames.offer(new MusicTracks(4, "Three Waves", "Ahoy"));
        trackNames.offer(new MusicTracks(5, "Lost Empire (Reprise)", "Ahoy"));

        Iterator<MusicTracks> iter = trackNames.iterator();
        while (iter.hasNext()) {
            System.out.println(trackNames.poll());
        }

        System.out.println("\n---[ Set: HashSet ]---\n- Group 5 members -");

        Set<String> groupMembers = new HashSet<>();
        groupMembers.add("Dario Portocarrero");
        groupMembers.add("Derek Long");
        groupMembers.add("Leonidas Yopan");
        groupMembers.add("Nathaniel May");

        for (String str : groupMembers) {
            System.out.println(str);
        }

        System.out.println("""

                ---[ TreeSet ]---
                - First, initial data -
                14 46 51 76 91 89 50 15 22 75 82 9 80 68 69 53 18 3 71 74

                - Same data entered into tree in same order -""");

        Set<Integer> treeData = new TreeSet<>();
        treeData.add(14);
        treeData.add(46);
        treeData.add(51);
        treeData.add(76);
        treeData.add(91);
        treeData.add(89);
        treeData.add(50);
        treeData.add(15);
        treeData.add(22);
        treeData.add(75);
        treeData.add(82);
        treeData.add(9);
        treeData.add(80);
        treeData.add(68);
        treeData.add(69);
        treeData.add(53);
        treeData.add(18);
        treeData.add(3);
        treeData.add(71);
        treeData.add(74);

        for (Integer number : treeData) {
            System.out.print(number + " ");
        }
        System.out.println();

        System.out.println("\n--[ Map ]--");
        Map lyrics = new HashMap<Integer, String>();
        lyrics.put(1,"We");
        lyrics.put(2,"wish");
        lyrics.put(3,"yuo");
        lyrics.put(4,"a");
        lyrics.put(5,"Merry");
        lyrics.put(6,"Christmas");
        lyrics.put(3,"you");

        for (int i = 1; i < 7; i++) {
            System.out.print(lyrics.get(i) + " ");
        }

        System.out.println();
    }
}

class TypeString implements Comparable<TypeString> {
    private final String words;
    public TypeString(String name) {
        this.words = name;
    }
    public String toString() {
        return words;
    }
    public int compareTo(TypeString ts) {
        return this.words.compareTo(ts.words);
    }
}

class TypeStringCompare implements Comparator<TypeString> {

    public int compare(TypeString a, TypeString b) {
        return a.toString().compareTo(b.toString());
    }
}

class MusicTracks {

    private final Integer number;
    private final String title;
    private final String artist;

    public MusicTracks(Integer number, String title, String artist) {
        this.number = number;
        this.title = title;
        this.artist = artist;
    }

    public String toString() {
        return number + ". " + title + " - " + artist;
    }
}


