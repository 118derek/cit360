package edu.byui.derekrl.w03;

import java.util.*;

public class Week03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input;
        Integer numerator = null;
        Integer denominator = null;
        Integer quotient = null;
        int counter = 0;

        do {
            try {
                System.out.print("Input numerator: ");
                input = scanner.nextLine();
                numerator = Integer.parseInt(input);
            } catch (NumberFormatException e) {
                System.out.println("Numerator must be an integer\n");
            }
            counter++;
            if (counter >= 3) {
                System.out.println("Too many wrong inputs, exiting");
                System.exit(1);
            }
        } while (numerator == null);

        do {
            try {
                counter = 0;

                do {
                    try {
                        System.out.print("Input denominator: ");
                        input = scanner.nextLine();
                        denominator = Integer.parseInt(input);
                    } catch (NumberFormatException e) {
                        System.out.println("Denominator must be an integer\n");
                    }
                    counter++;
                    if (counter >= 3) {
                        System.out.println("Too many wrong inputs, exiting");
                        System.exit(1);
                    }
                } while (denominator == null);

                quotient = Division.doDivision(numerator, denominator);
            } catch (ArithmeticException e) {
                System.out.println("Denominator cannot be zero\n");
            }
        } while (quotient == null);

        System.out.printf("Will calculate %d / %d\n", numerator, denominator);
        System.out.printf("Result is %d\n", quotient);
        scanner.close();
    }


}

class Division {
    static int doDivision(int numerator, int denominator) throws ArithmeticException {
        return numerator / denominator;
    }
}

