package edu.byui.derekrl.w03;

import java.util.*;

public class Week03Rewrite {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input;
        Float numerator = null;
        Float denominator = null;
        Float test = null;
        Float quotient = null;
        int counter = 0;

        do {
            System.out.print("Input numerator: ");
            if (scanner.hasNextFloat()) {
                input = scanner.nextLine();
                try {
                    numerator = Float.parseFloat(input);
                } catch (NumberFormatException e) {
                    System.out.println("Numerator must be a number\n");
                    counter++;
                }
            } else {
                scanner.nextLine();
                System.out.println("Numerator must be a number\n");
                counter++;
                if (counter >= 3) {
                    System.out.println("Too many wrong inputs, exiting");
                    System.exit(1);
                }
            }
        } while (numerator == null);

        counter = 0;
        do {
            System.out.print("Input denominator: ");
            if (scanner.hasNextFloat()) {
                input = scanner.nextLine();
                try {
                    test = Float.parseFloat(input);
                    if (test == 0) {
                        System.out.println("Denominator cannot be zero\n");
                        counter++;
                        continue;
                    } else {
                        denominator = test;
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Denominator must be a number\n");
                    counter++;
                }
            } else {
                scanner.nextLine();
                System.out.println("Denominator must be a number\n");
                counter++;
            }

            if (counter >= 3) {
                System.out.println("Too many wrong inputs, exiting");
                System.exit(1);
            }

        } while (denominator == null);

        System.out.printf("Will calculate %s / %s\n", numerator, denominator);
        // Float.toString() because unformatted floats are ugly and this is simple

        try {
            quotient = DivisionInRewrite.doDivision(numerator, denominator);
        } catch (ArithmeticException e) {
            System.out.println("I don't know how you managed to break this, but you did. Congratulations");
            System.exit(1);
        }

        System.out.printf("Result is %s\n", quotient);
        scanner.close();
    }
}

class DivisionInRewrite {
    static float doDivision(Float numerator, Float denominator) throws ArithmeticException {
        return numerator / denominator;
    }
}
