package edu.byui.derekrl.w08;

import java.util.Random;

public class Patron implements Runnable {
    private String name;
    private Book theBook;
    private int rand;

    public Patron(String name, Book book) {
        this.name = name;
        this.theBook = book;
        Random random = new Random();
        this.rand = random.nextInt(9000) + 1;
    }

    public void run() {
        System.out.println(name + " has arrived at the library. They will be at the desk in " + rand + " milliseconds.");
        try {
            Thread.sleep(rand);
        } catch (InterruptedException e) {
            System.err.println(e.toString());
        }
        System.out.println("[!] " + name + " is now at the desk and wants the book.");
        theBook.checkOut(name);
    }
}
