package edu.byui.derekrl.w08;

public class Book {

    public Book(){}

    public synchronized void checkOut(String patron) {
            System.out.println("--> " + patron + " has checked out the book.");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                System.err.println(e.toString());
            }
            System.out.println("<-- " + patron + " finished the book and has checked it back in.");
    }
}
