package edu.byui.derekrl.w08;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Library {

    public static void main(String[] args) {

        Book theBook = new Book();
        ExecutorService queue = Executors.newFixedThreadPool(3);

        Patron pat1 = new Patron("Jameson", theBook);
        Patron pat2 = new Patron("Watts", theBook);
        Patron pat3 = new Patron("Annabelle", theBook);
        Patron pat4 = new Patron("Biggie T. Spoon", theBook);
        Patron pat5 = new Patron("Cornelius", theBook);
        Patron pat6 = new Patron("Josephine", theBook);
        Patron pat7 = new Patron("Theodore", theBook);

        queue.execute(pat1);
        queue.execute(pat2);
        queue.execute(pat3);
        queue.execute(pat4);
        queue.execute(pat5);
        queue.execute(pat6);
        queue.execute(pat7);

        queue.shutdown();
    }
}
