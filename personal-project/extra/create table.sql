CREATE TABLE restaurants(
   id        INT 
  ,name      VARCHAR(40)
  ,cuisine   INT 
  ,address   VARCHAR(40)
  ,latitude  FLOAT(10,4)
  ,longitude FLOAT(10,4)
  ,PRIMARY KEY (id)
);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (1,'180 Tacos and Global Street Eats',3,'3368 N University Ave',40.2786,-111.6578);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (2,'Backdoor BRGR',5,'261 N University Ave',40.2372,-111.6587);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (3,'Bajio Mexican Grill',3,'4801 N University Ave #51',40.2979,-111.6595);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (4,'Beto’s Mexican Food',3,'1314 N State St',40.2531,-111.669);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (5,'Black Sheep Cage',1,'19 N University Ave',40.2341,-111.6589);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (6,'Block Restaurant',1,'3330 N University Ave',40.2784,-111.6577);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (7,'Bonsai Sushi',2,'672 Freedom Blvd 200W',40.2427,-111.662);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (8,'BRASAS MEXICAN GRILL LLC',3,'227 Cougar Blvd',40.2505,-111.6625);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (9,'Braseras Mexican Grill',3,'238 W 100 S',40.2326,-111.6626);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (10,'Brick Oven Provo',4,'111 E 800 N',40.2444,-111.6564);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (11,'Bumblebee’s KBBQ and Grill',5,'1254 N State St',40.2511,-111.6775);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (12,'Burgers Surpreme',1,'1796 University Pkwy',40.2582,-111.6619);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (13,'Cafe Rio Mexican Grill',3,'2250 N University Pkwy Ste 816',40.2642,-111.6685);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (14,'Cafe Rio Mexican Grill',3,'1122 S University Ave',40.2185,-111.659);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (15,'Cafe Zupas',1,'408 W 2230 N',40.2636,-111.6651);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (16,'CHOM Burger',1,'45 300 N',40.2374,-111.6586);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (17,'Chuck-A-Rama Buffet',1,'1081 S University Ave',40.2192,-111.6588);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (18,'Costa Vida',3,'1200 N University',40.2502,-111.6584);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (19,'Cubby’s',5,'1258 N State St',40.2513,-111.6676);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (20,'Cupbop – Korean BBQ',2,'815 N 700 E',40.2234,-111.6617);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (21,'Demae Japanese Restaurant',2,'82 W Center St',40.234,-111.6601);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (22,'Denny’s',1,'1680 Freedom Blvd 200 W',40.2566,-111.6617);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (23,'Dirty Bird Chxx',5,'495 E 600 N',40.2419,-111.6502);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (24,'DP CHEESESTEAKS',1,'4801 N University Ave Suite 680',40.2979,-111.6595);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (25,'El Gallo Giro',3,'346 N University Ave',40.2383,-111.6583);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (26,'El Huarache Sabroson',3,'1700 N State St',40.2566,-111.6725);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (27,'El Mexsal',3,'325 S 200 W',40.2293,-111.6621);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (28,'El Taco-Nazo',3,'2048 W Center St',40.2339,-111.694);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (29,'Fat Daddy’s Pizzeria',4,'223 W Center St',40.2335,-111.6626);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (30,'Five Sushi Brothers',2,'445 Freedom Blvd 200W',40.2397,-111.6625);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (31,'Four Seasons Hot Pot & Dumplings',2,'236 N University Ave',40.2369,-111.6585);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (32,'Good Grindz Hawaiian Cuisine',5,'1200 Towne Centre Blvd Unit 2068',40.2168,-111.6636);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (33,'Good Move Cafe',1,'1 E Center St Ste 100',40.2338,-111.6585);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (34,'Guru’s Cafe',5,'45 E Center St',40.234,-111.6578);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (35,'Happy Sumo',2,'4801 N University Ave',40.2979,-111.6595);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (36,'Hungry Hawaiian Provo',5,'180 N University Ave Ste 135',40.2361,-111.6585);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (37,'J Dawgs',1,'858 N 700 E',40.2452,-111.6563);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (38,'JJ Burger',1,'40 N 400 W',40.2343,-111.6653);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (39,'Joe Vera’s Mexican Restaurant',3,'201 W Center St',40.2335,-111.6623);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (40,'Katsu City',2,'1700 N State St',40.2566,-111.6725);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (41,'Koi Ramen Provo',2,'1283 N University Ave #102',40.2515,-111.6587);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (42,'Koko Lunchbox',2,'1175 N Canyon Rd #3420',40.2495,-111.6562);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (43,'Kokonut Island Grill Provo',5,'62 Cougar Blvd',40.2506,-111.6596);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (44,'L&L Hawaiian Barbecue – Provo',5,'158 Cougar Blvd',40.2526,-111.6612);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (45,'LA CATRINA MEXICAN GRILL',3,'3161 Canyon Rd',40.2935,-111.653);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (46,'La Jolla Groves',1,'4801 N University Ave Suite 610',40.2979,-111.6595);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (47,'Little Suite Provo',5,'1380 S University Ave',40.215,-111.6603);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (48,'Lovebirds Hot Chicken',5,'1841 N State St',40.2581,-111.6741);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (49,'Lovely Pho',2,'2304 N University Pkwy',40.2648,-111.6702);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (50,'Magleby’s Fresh',1,'3362 N University Ave',40.2786,-111.6578);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (51,'Malawi’s Pizza Provo Riverwoods',4,'4801 N University Ave STE #110',40.2979,-111.6595);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (52,'Mi Link Guadalajara',3,'446 N Freedom Blvd',40.2396,-111.662);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (53,'Molly’s',1,'753 W Columbia Ln',40.2539,-111.6735);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (54,'MOOYAH Burgers, Fries & Shakes',1,'62 W 1230 N St Suite 105',40.2506,-111.6596);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (55,'Mozz Artisan Pizza',4,'145 N University Ave',40.2356,-111.6591);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (56,'Nico’s Pizza',4,'255 Cougar Blvd',40.2505,-111.6625);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (57,'Noodles and Company',2,'62 West Bulldog Blvd',40.2474,-111.715);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (58,'Panda Express',2,'1240 N University Ave',40.2169,-111.659);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (59,'Pho Plus',2,'68 W Center St',40.237,-111.6598);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (60,'Pita Pit',5,'1240 N University Ave B',40.2169,-111.659);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (61,'Pizza Pie Cafe',4,'2235 N University Pkwy',40.2609,-111.6645);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (62,'Quiero Mas – Mexican Cafe',3,'155 N University Ave',40.2358,-111.6587);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (63,'Real Tamales',3,'610 W Center St',40.2338,-111.6685);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (64,'Red Robin Gourmet Burgers and Brews',5,'1200 Towne Centre Blvd Suite 1100',40.2168,-111.6636);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (65,'Ruby River Steakhouse',5,'1454 S University Ave',40.214,-111.6594);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (66,'Saigon Cafe',2,'440 W 300 S',40.2299,-111.6664);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (67,'Sam Hawk',2,'684 N Freedom Blvd',40.2428,-111.662);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (68,'Sensuous Sandwich',5,'163 W Center St',40.2333,-111.6615);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (69,'Seven Brothers Burgers',1,'4801 N University Ave',40.2991,-111.6575);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (70,'Shoots Chinese Restaurant',2,'4801 N University Ave',40.2991,-111.6575);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (71,'Silver Dish Thai Cuisine',2,'278 W Center St',40.234,-111.6335);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (72,'Sizzler',5,'1385 S University Ave',40.2149,-111.6588);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (73,'SLABpizza',4,'671 E 800 N',40.2443,-111.647);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (74,'Spicy Thai',2,'3230 N University Ave',40.2777,-111.6577);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (75,'Station 22',1,'22 W Center St',40.234,-111.6592);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (76,'Sushi Burrito Provo',2,'283 E 300 S',40.2297,-111.6538);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (77,'Taqueria San Marcos',3,'491 S Freedom Blvd #104',40.2401,-111.6622);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (78,'Thai Hut',2,'410 N University Ave',40.2281,-111.6588);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (79,'Thai Neighbor Cuisine 1',2,'170 W 300 S',40.2298,-111.6616);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (80,'Thai Papaya Cuisine',5,'1774 N University Pkwy',40.2576,-111.6605);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (81,'The Great Steak Grill',5,'1650 W Center St',40.234,-111.6882);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (82,'The Spoon',5,'75 W Center St',40.2333,-111.6606);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (83,'Tommy’s Burgers',1,'401 W 100 N',40.2349,-111.6658);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (84,'Two Jack’s Pizza',4,'80 W Center St',40.234,-111.66);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (85,'Vegan Sun',5,'225 W Center St',40.2335,-111.6626);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (86,'Village Inn',1,'933 S University Ave',40.2214,-111.6584);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (87,'Waffle Love – Provo',1,'1831 N State St',40.258,-111.674);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (88,'Wild Ginger',2,'366 N University Ave',40.2385,-111.6583);
INSERT INTO restaurants(id,name,cuisine,address,latitude,longitude) VALUES (89,'WINGERS Restaurant',5,'1200 Towne Centre Blvd #1096',40.2168,-111.6636);

