package derekrl.personalproject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class HibRetrieval {

    SessionFactory factory = null;
    Session session = null;

    private static HibRetrieval single_instance = null;

    private HibRetrieval() {factory = HibUtilities.getSessionFactory();}

    public static HibRetrieval getInstance() {
        if (single_instance == null) {
            single_instance = new HibRetrieval();
        }
        return single_instance;
    }

    public List<Restaurant> getAllRestaurants() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "from derekrl.personalproject.Restaurant";
            List<Restaurant> restaurants = (List<Restaurant>)session.createQuery(query).getResultList();
            session.getTransaction().commit();
            return restaurants;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public List<Restaurant> getSpecificRestaurants(int cuisine) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "from derekrl.personalproject.Restaurant where cuisine=" + Integer.toString(cuisine);
            List<Restaurant> restaurants = (List<Restaurant>)session.createQuery(query).getResultList();
            session.getTransaction().commit();
            return restaurants;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}
