package derekrl.personalproject;

import org.apache.commons.text.StringEscapeUtils;
import javax.persistence.*;

@Entity
@Table(name = "restaurants")

public class Restaurant implements Comparable<Restaurant> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "cuisine")
    private int cuisine;

    @Column(name = "address")
    private String address;

    @Column(name = "latitude")
    private float latitude;

    @Column(name = "longitude")
    private float longitude;

    @Transient
    private Float distance;

    @Transient
    private boolean escaped = false;

    public int getId() {return id;}
    public void setId(int id) {this.id = id;}
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public int getCuisine() {return cuisine;}
    public void setCuisine(int cuisine) {this.cuisine = cuisine;}
    public String getAddress() {return address;}
    public void setAddress(String address) {this.address = address;}
    public float getLatitude() {return latitude;}
    public void setLatitude(float latitude) {this.latitude = latitude;}
    public float getLongitude() {return longitude;}
    public void setLongitude(float longitude) {this.longitude = longitude;}
    public boolean isEscaped() {return escaped;}
    public void setEscaped(boolean escaped) {this.escaped = escaped;}
    public Float getDistance() {return distance;}
    public void setDistance(Float distance) {this.distance = distance;}

    public void escapeStrings() {
        if (!this.escaped) {
            this.name = StringEscapeUtils.escapeHtml4(this.name);
            this.address = StringEscapeUtils.escapeHtml4(this.address);
            this.escaped = true;
        }
    }

    public void unescapeStrings() {
        if (this.escaped) {
            this.name = StringEscapeUtils.unescapeHtml4(this.name);
            this.address = StringEscapeUtils.unescapeHtml4(this.address);
            this.escaped = false;
        }
    }

    public void calculateDistance(float latitude, float longitude) {
        float varA = (float) Math.pow(latitude - this.latitude, 2);
        float varB = (float) Math.pow(longitude - this.longitude, 2);
        distance = (float) Math.sqrt(varA + varB);
    }

    @Override
    public int compareTo(Restaurant r) {
        if (r.getDistance() > this.getDistance()) {
            return -1;
        } else if (r.getDistance() < this.getDistance()) {
            return 1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return String.format("Name: %s Address: %s Lat: %f Lon: %f Dist: %f", name, address, latitude, longitude, distance);
    }
}
