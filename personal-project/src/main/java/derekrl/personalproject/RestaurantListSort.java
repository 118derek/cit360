package derekrl.personalproject;

import java.util.*;
import java.util.concurrent.Callable;

public class RestaurantListSort implements Callable<List<Restaurant>> {

    private final List<Restaurant> list;

    public RestaurantListSort(List<Restaurant> list) {
        this.list = list;
    }

    public void sort(List<Restaurant> list) {
        Collections.sort(list);
    }

    @Override
    public List<Restaurant> call() throws Exception {
        sort(list);
        return list;
    }
}
