package derekrl.personalproject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})

public class Servlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        float gpsx = Float.parseFloat(request.getParameter("gps-x"));
        float gpsy = Float.parseFloat(request.getParameter("gps-y"));
        int cuisine = Integer.parseInt(request.getParameter("cuisine"));

        HibRetrieval retrieval = HibRetrieval.getInstance();
        List<Restaurant> restaurantList;

        if (cuisine == 0) {
            restaurantList = retrieval.getAllRestaurants();
        } else {
            restaurantList = retrieval.getSpecificRestaurants(cuisine);
        }
        StringBuilder output = new StringBuilder();

        output.append("<h1>List of the five closest ");
        switch(cuisine) {
            case 0:
                break;
            case 1:
                output.append("American ");
                break;
            case 2:
                output.append("Asian ");
                break;
            case 3:
                output.append("Mexican ");
                break;
            case 4:
                output.append("pizza ");
                break;
            case 5:
                output.append("variety ");
                break;
            default:
                throw new ServletException("Invalid cuisine parameter");
        }
        output.append("restaurants in the area:</h1><p>");

        for (Restaurant i : restaurantList) {
            i.escapeStrings();
            i.calculateDistance(gpsx, gpsy);
        }

        ExecutorService queue = Executors.newSingleThreadExecutor();
        RestaurantListSort sorter = new RestaurantListSort(restaurantList);
        Future<List<Restaurant>> future = queue.submit(sorter);
        try {
            restaurantList = future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        queue.shutdown();
        List<Restaurant> sortedRestaurantsList = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            sortedRestaurantsList.add(restaurantList.get(i));
        }

        for (Restaurant i : sortedRestaurantsList) {
//            output.append(String.format("<br><strong>%s</strong> - %s<br>%f<br>s %f %f<br>i %f %f<br>",
//                    i.getName(), i.getAddress(), i.getDistance(), gpsx, gpsy, i.getLatitude(), i.getLongitude()));
            output.append(String.format("<br><strong>%s</strong><br>%s<br>",i.getName(), i.getAddress()));
        }

        out.println("<html><head></head><body>");
        out.println(output);
        out.println("</body></html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly. Use input.html");
    }
}