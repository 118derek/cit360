package derekrl.personalproject;

import java.util.*;

public class HibTest {

    public static void main(String[] args) {
        HibRetrieval retrieval = HibRetrieval.getInstance();

        List<Restaurant> restaurantList = retrieval.getSpecificRestaurants(4);

        System.out.println("List of all pizza restaurants in the database");
        for (Restaurant i : restaurantList) {
            i.escapeStrings();
            System.out.printf("%s - %s%n", i.getName(), i.getAddress());
        }
    }
}
