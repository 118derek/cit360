package week07;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})

public class Servlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        String input = request.getParameter("colors");

        int count = 0;
        StringBuilder output;

        Map<String, Boolean> checks = new HashMap<>();
        checks.put("blue", false);
        checks.put("green", false);
        checks.put("orange", false);
        checks.put("red", false);
        checks.put("white", false);
        checks.put("yellow", false);

        for (String i : checks.keySet()) {
            if (input.toLowerCase().contains(i)) {
                checks.put(i,true);
                count++;
            }
        }

        switch(count) {
            case 6:
                output = new StringBuilder("That's all of them!");
                break;
            case 5:
                output = new StringBuilder("Almost, you only missed:");
                break;
            case 4:
            case 3:
                output = new StringBuilder("That was some of them, but you still missed these:");
                break;
            case 2:
            case 1:
                output = new StringBuilder("There are many more colors than that, such as these:");
                break;
            default:
                output = new StringBuilder("Here's what the correct colors are:");
        }

        for (String i : checks.keySet()) {
            if (!checks.get(i)) {
                output.append("<br>").append(i);
            }
        }

        out.println("<html><head></head><body>");
        out.println("<h1>Result</h1>");
        out.println("<p>" + output + "</p>");
        out.println("<p>Correct colors: " + count + "</p>");
        out.println("</body></html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly. Use input.html");
    }

}
