package edu.derekrl.w09;

import java.util.*;

public class GenerateTrackList {

    public static void main(String[] args) {

        Retrieval rtr = Retrieval.getInstance();

        List<Track> tracks = rtr.getAllTracks();

        System.out.println("List of all tracks in the generator:");
        for (Track i : tracks) {
            System.out.println(i);
        }

        System.out.println("\nSelecting three random tracks");
        Collections.shuffle(tracks);

        System.out.println("\nTracks to race this session:");
        for (int i = 0; i < 3; i++) {
            System.out.println(tracks.get(i));
        }

    }

}

/*
## SQL TABLE CREATION ##

create table tracks (
    id int primary key not null auto_increment,
    name varchar(64),
    difficulty varchar(16),
    length int,
    laps int);

insert into tracks values (null, "Holiday Camp", "Medium", 1170, 2);
insert into tracks values (null, "Jailhouse Rock", "Extreme", 729, 3);
insert into tracks values (null, "Sakura", "Hard", 752, 3);
insert into tracks values (null, "Ghost Town", "Medium", 752, 3);
insert into tracks values (null, "Toy World Mayhem", "Hard", 645, 4);
insert into tracks values (null, "Petrovolt", "Easy", 948, 2);
 */
