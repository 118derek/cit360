package edu.derekrl.w09;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class Retrieval {

    SessionFactory factory = null;
    Session session = null;

    private static Retrieval single_instance = null;

    private Retrieval() {factory = Utilities.getSessionFactory();}

    public static Retrieval getInstance() {
        if (single_instance == null) {
            single_instance = new Retrieval();
        }
        return single_instance;
    }

    public List<Track> getAllTracks() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "from edu.derekrl.w09.Track";
            List<Track> tracks = (List<Track>)session.createQuery(query).getResultList();
            session.getTransaction().commit();
            return tracks;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}
